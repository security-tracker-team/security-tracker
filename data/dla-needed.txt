An LTS security update is needed for the following source packages.

To add a new entry, please coordinate with this week's Front-Desk
person, and use the 'package-operations' LTS tool.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

A note to Freexian contributors/collaborators: when selecting what
package to work on first, please use:
  ./find-work
  https://freexian.gitlab.io/services/deblts-team/documentation/lts/information-for-lts-contributors.html
  (private for now)
to sort packages by priority and display important notes about the
package (special attention, VCS, testing procedures, programming
language, maintainers to coordinate with, etc.).

To work on a package, simply add your name behind it. To learn more about how
this list is updated have a look at
https://lts-team.pages.debian.net/wiki/Development.html#triage-new-security-issues

To make it easier to see the entire history of an update, please append notes
rather than remove/replace existing ones.

--
abseil
  NOTE: 20250309: Added by Front-Desk (rouca)
--
amd64-microcode
  NOTE: 20250208: Added by Front-Desk (apo)
--
ansible
  NOTE: 20240915: Added by Front-Desk (ta)
  NOTE: 20241103: Fixed sid, bookworm, and bullseye (rouca)
  NOTE: 20241103: Bullseye autopkgtest fail (unrelated to fix) try to fix before release (rouca)
  NOTE: 20241120: Waiting for release by Lee testsuite is ok (rouca)
  NOTE: 20241123: Made a partial release. only CVE-2024-11079 needed but more upstream backport work needed (rouca)
--
arm-trusted-firmware
  NOTE: 20250303: Added by Front-Desk (rouca)
--
ceph
  NOTE: 20241205: Added by Front-Desk (santiago)
  NOTE: 20241205: maintainer is preparing an update: https://lists.debian.org/debian-lts/2024/12/msg00008.html (santiago/front-desk)
  NOTE: 20241221: Liasing with maintainer. (lamby)
  NOTE: 20241231: Reviewing package with maintainer. (lamby)
--
ckeditor
  NOTE: 20241002: Added by Front-Desk (Beuc)
  NOTE: 20241002: Multiple CVEs have been piling up (Beuc/front-desk)
--
ckeditor3
  NOTE: 20241121: Added by Front-Desk (Beuc)
  NOTE: 20241121: Only used by Horde editor (Beuc/front-desk)
  NOTE: 20241002: rouca to check EOL'd ckeditor3 -> ckeditor[v4] upgrade path
  NOTE: 20241002: https://lists.debian.org/debian-lts/2024/10/msg00003.html
--
dcmtk
  NOTE: 20250220: Added by Front-Desk (Beuc)
  NOTE: 20250220: Previous DLA introduced another regression, this is CVE-2024-47796.
  NOTE: 20250220: New CVEs were released.
  NOTE: 20250220: Follow/contribute to in-progress PU #1095854 (Beuc/front-desk)
  NOTE: 20250224: See https://salsa.debian.org/lts-team/packages/dcmtk/-/commits/wip/bullseye (ah)
--
edk2
  NOTE: 20240815: Added by Front-Desk (Beuc)
  NOTE: 20240815: bullseye did not get most of DSA 5624-1 security fixes,
  NOTE: 20240815: (10 ipv6-related, postponed CVEs), plus there are older postponed vulnerabilities (Beuc/front-desk)
  NOTE: 20241105: maintainer proposed opu debdiff for CVE-2024-38796 and CVE-2024-1298, https://bugs.debian.org/1086762 (santiago)
--
espeak-ng
  NOTE: 20240816: Added by Front-Desk (Beuc)
  NOTE: 20240816: Follow fixes from bookworm 12.5 (5 CVEs) (Beuc/front-desk)
  NOTE: 20240929: Upstream patches not enough to fix issues in bullseye. (abhijith)
  NOTE: 20240929: Can be still reproduced (abhijith)
  NOTE: 20241014: Still looking at the incomplete fixes (abhijith)
  NOTE: 20241104: haven't spend time to look in to it. Will look after fixing puma (abhijith)
--
fastdds
  NOTE: 20250303: Added by Front-Desk (rouca)
--
firmware-nonfree
  NOTE: 20241011: Added by Front-Desk (pochu)
  NOTE: 20241011: Update to bookworm version, possibly coordinate upload of
  NOTE: 20241011: trixie version to bookworm-pu and backport that to bullseye (pochu)
  NOTE: 20241020: started discussion on how generally approach this package,
  NOTE: 20241020: Message-ID: <ZxTjEflb-ssaTmA8@isildor2.loewenhoehle.ip> / "Re: Update on firmware-nonfree" (tobi)
  NOTE: 20241117: and <ZzovRz-UIif8e69i@localhost> / "How to handle firmware for LTS (and ELTS) [WAS: Re: Update on firmware-nonfree]" (Beuc/front-desk)
--
flatpak
  NOTE: 20240814: Added by oldstable Security Team (carnil)
  NOTE: 20240815: Follow fixes from DSA-5749-1 (CVE-2024-42472) (Beuc/front-desk)
  NOTE: 20241002: See also https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1082927 (Beuc/front-desk)
--
freeimage
  NOTE: 20240922: Added by Front-Desk (apo)
  NOTE: 20240922: Many postponed CVE.
  NOTE: 20241202: still WIP (santiago)
--
fwupd
  NOTE: 20250217: Added by Front-Desk (Beuc)
--
graphicsmagick
  NOTE: 20250307: Added by Front-Desk (rouca)
  NOTE: 20250307: Please check if CVEs apply also to imagemagik (rouca)
--
grub2
  NOTE: 20250105: Added by Front-Desk (apo)
  NOTE: 20250105: high-profile package but not enough details yet. (apo)
  NOTE: 20250219: New batch of 21 CVEs, with fixes (Beuc/front-desk)
--
intel-microcode
  NOTE: 20250217: Added by Front-Desk (Beuc)
  NOTE: 20250217: Fixes are being tested in unstable,
  NOTE: 20250217: (CVE-2023-34440..CVE-2024-39355, 12 CVEs)
  NOTE: 20250217: Follow stable if included in upcoming bookworm 12.10. (Beuc/front-desk)
--
ipmctl
  NOTE: 20250112: Added by Front-Desk (ta)
  NOTE: 20250217: I wasn't able to determine a patch for CVE-2023-27517 for any of the series (dleidert)
--
jetty9
  NOTE: 20241110: Added by Front-Desk (apo)
  NOTE: 20250224: Update is ready based on 9.4.56. Intend to mark CVE-2024-6763 and CVE-2024-6763
  NOTE: 20250224: as ignored. Feature is either deprecated, not a Jetty server problem or can be worked around.
--
jinja2
  NOTE: 20250105: Added by Front-Desk (apo)
  NOTE: 20250122: Updated sid, waiting for ci.debian.net results.  (spwhitton)
  NOTE: 20250122: CVE-2024-56201 is not directly affected (no f-syntax in Python2),
  NOTE: 20250122: to be double-checked whether similar vulnerability exists in the old code.
  NOTE: 20250122: CVE-2024-56326 testcase does not work directly in bullseye.
  NOTE: 20250122: Don't break the Python2 package again as I did (DLA-3988-2). (bunk)
--
knot-resolver
  NOTE: 20240924: Added by Front-Desk (lamby)
--
lemonldap-ng (dleidert)
  NOTE: 20250206: CVE-2024-52948
--
libcap2 (Chris Lamb)
  NOTE: 20250220: Added by Front-Desk (Beuc)
  NOTE: 20250220: Also fix postponed/no-dsa issues (Beuc/front-desk)
  NOTE: 20250306: Update prepared, just clarifying one last CVE (CVE-2023-2603). (lamby)
--
libdatetime-timezone-perl (Emilio)
  NOTE: 20250303: Added by pochu
--
libnet-easytcp-perl
  NOTE: 20250117: Added by Front-Desk (rouca)
--
libreoffice
  NOTE: 20250304: Added by Front-Desk (rouca)
--
linux (Ben Hutchings)
  NOTE: 20230111: Perma-added, Linux package specifically delegated to bwh (LTS Team)
--
mina2
  NOTE: 20250111: Added by Front-Desk (ta)
  NOTE: 20250114: Patches for CVE-2024-52046 https://github.com/apache/mina/commit/f9cc5ada6ebef4ee7cc51aac824e42e2e422310e (2.2.4) and ... (dleidert)
  NOTE: 20250114: ... https://github.com/apache/mina/commit/cdb59eb6131696a440870ab89ad0e20804eb5ca7 (2.1.10) (dleidert)
--
musl (Utkarsh)
  NOTE: 20250217: Added by Front-Desk (Beuc)
  NOTE: 20250218: Requested review. (lamby)
  NOTE: 20250219: Update delayed until CVE-2025-26519 fixed in unstable. (lamby)
  NOTE: 20250219: → See "Re: Please review musl 1.2.2-1+deb11u1 for bullseye LTS" on debian-lts@lists.debian.org. (lamby)
--
nagvis
  NOTE: 20250117: Added by Front-Desk (rouca)
  NOTE: 20250119: Also check/fix https://bugs.debian.org/1061044
  NOTE: 20250119: when testing your fix for bookworm. (bunk)
  NOTE: 20250221: https://salsa.debian.org/lts-team/lts-updates-tasks/-/issues/193 (ah)
--
nginx (andrewsh)
  NOTE: 20250207: Added by Front-Desk (apo)
--
node-axios
  NOTE: 20250308: Added by Front-Desk (rouca)
--
node-prismjs
  NOTE: 20250303: Added by Front-Desk (rouca)
--
nvidia-cuda-toolkit
  NOTE: 20241004: Added by Front-Desk (Beuc)
--
odoo
  NOTE: 20250303: Added by Front-Desk (rouca)
--
openafs
  NOTE: 20241207: Added by Front-Desk (santiago)
  NOTE: 20250102: Looking at CVE-2024-10394 (abhijith)
  NOTE: 20250203: https://people.debian.org/~abhijith/upload/openafs_patches/ (abhijith)
--
openjpeg2
  NOTE: 20250105: Added by Front-Desk (apo)
  NOTE: 20250224: Discovered two regressions. I plan to release on Wednesday. (apo)
--
pagure
  NOTE: 20250117: Added by Front-Desk (rouca)
  NOTE: 20250119: Coordinate with ds (rouca/FD)
  NOTE: 20250216: Prepared patches in lts-team/packages/pagure, but two issues here (dleidert)
  NOTE: 20250216: 1) The test results are ignored and there is no way to determine any regression (dleidert)
  NOTE: 20250216: 2) yui-compressor fails with multiple JS files (Can't find bundle for base name org.mozilla.javascript.resources.Messages) (dleidert)
  NOTE: 20250216: The second issue is outside of my field of expertise. Returning to pool and send message to list (dleidert)
  NOTE: 20250217: Upcoming DSA, coordinate with security team (Beuc/front-desk)
--
pgagent
  NOTE: 20250117: Added by Front-Desk (rouca)
--
php-laravel-framework
  NOTE: 20250307: Added by Front-Desk (rouca)
--
php-twig (guilhem)
  NOTE: 20250209: Added by Front-Desk (apo)
  NOTE: 20250209: Vulnerable code is in src/Node/Expression/NullCoalesceExpression.php (apo)
--
phpmyadmin (Chris Lamb)
  NOTE: 20250209: Added by Front-Desk (apo)
  NOTE: 20250219: Packaged prepared on salsa. (lamby)
  NOTE: 20250306: Checking some postponed issues. (lamby)
--
python-django (Chris Lamb)
  NOTE: 20250307: Added by Front-Desk (rouca)
--
python3.9
  NOTE: 20250303: Added by Front-Desk (rouca)
  NOTE: 20250309: Fix CVE-2025-0938, investigate CVE-2022-0391 and wait for upstream of CVE-2025-1795 (rouca)
--
qemu
  NOTE: 20240815: Added by Front-Desk (Beuc)
  NOTE: 20240815: Follow fixes from bookworm 12.4 (CVE-2023-5088)
  NOTE: 20240815: Follow fixes from bookworm 12.5 (CVE-2023-3019, CVE-2023-6693)
  NOTE: 20240815: Follow fixes from bookworm 12.6 (CVE-2024-3446,CVE-2024-3447)
  NOTE: 20240815: CVE-2024-4467 fix also proposed for 12.7 (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1076504)
  NOTE: 20241119: Bookworm PU in progress https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1086572
  NOTE: 20241227: WIP
  NOTE: 20250108: Still trying to reproduce CVE-2024-3446. According to upstream, it seems it is possible (santiago)
--
rails
  NOTE: 20250105: Added by Front-Desk (apo)
  NOTE: 20250305: Utkarsh uploaded the CVE fixes to unstable via rails/7.2.2.1. (utkarsh)
--
rsync
  NOTE: 20250121: Added by Front-Desk re. potential regression outlined in #1093696. (lamby)
  NOTE: 20250211: no upstream fix yet
--
rubygems
  NOTE: 20250304: Added by Front-Desk (rouca)
--
shadow
  NOTE: 20250105: Added by Front-Desk (apo)
  NOTE: 20250105: shadow is a high-profile package. Upstream discussion for CVE-2024-56433 is
  NOTE: 20250105: ongoing. I'm adding it to dla-needed.txt to keep it on our radar.
--
snapcast
  NOTE: 20250118: Added by Front-Desk (rouca)
  NOTE: 20250119: Upstream just re-added a secured Stream.AddStream functionality to fix CVE-2023-36177, but hasn't released it yet (dleidert)
  NOTE: 20250119: That seems to be a better fix than just removing the functionality as done in the initial patch (dleidert)
  NOTE: 20250119: Returning to pool until this has been tested and released into Sid/Trixie (dleidert)
--
sogo
  NOTE: 20240922: Added by Front-Desk (apo)
  NOTE: 20240922: See also postponed issues.
--
suricata (abhijith)
  NOTE: 20250112: Added by Front-Desk (ta)
--
symfony
  NOTE: 20241110: Added by Front-Desk (apo)
  NOTE: 20241120: Follow fixes from DSA-5809-1 and DSA-5813-1 (Beuc/front-desk)
  NOTE: 20241201: Contacted David Prévot for guidance regarding tests. (dleidert)
  NOTE: 20241201: During build, packages built by symfony are already installed and can lead to build/test failures. (dleidert)
--
tcpdf
  NOTE: 20241205: Added by Front-Desk (santiago)
  NOTE: 20241230: https://lists.debian.org/debian-lts/2024/12/msg00057.html (bunk)
--
trafficserver
  NOTE: 20241120: Added by Front-Desk (Beuc)
  NOTE: 20241120: Upcoming DSA (Beuc/front-desk)
  NOTE: 20241203: Upstream announcement does not mention 8.1 for any of the 3 CVEs.
  NOTE: 20241203: AFAIR upstream 8.1 support ended with the release of 10.0 (bunk)
  NOTE: 20250216: DLA released fixing CVE-2024-38479 and CVE-2024-50306 (dleidert)
  NOTE: 20250216: IMHO CVE-2024-50305 does not affect 8.x due to affected code being introduced later (dleidert)
  NOTE: 20250216: Bookworm-PU necessary, but issues not fixed in Sid yet; contacted maintainer (dleidert)
--
twitter-bootstrap3
  NOTE: 20241110: Added by Front-Desk (apo)
  NOTE: 20241119: Supportability discussion https://lists.debian.org/debian-lts/2024/11/msg00030.html (Beuc/front-desk)
--
tzdata (Emilio)
  NOTE: 20250303: Added by pochu
--
u-boot
  NOTE: 20250219: Added by Front-Desk (Beuc)
  NOTE: 20250219: New CVEs, plus it's time to fix all the no-dsa&postponed CVEs (Beuc/front-desk)
--
vim (Sean Whitton)
  NOTE: 20250114: Added by Front-Desk (rouca)
  NOTE: 20250129: Fixes for first 29 outstanding CVEs backported in
  NOTE: 20250129: salsa:lts-team/packages/vim.git#debian/bullseye.
  NOTE: 20250129: However, the tests do not pass yet.  (spwhitton)
  NOTE: 20250129: Re CVE-2024-22667: There are three sprintf calls sites which
  NOTE: 20250129: need changing to snprintf: two in did_set_string_option, and
  NOTE: 20250129: one in illegal_char.  The fourth call site changed in
  NOTE: 20250129: upstream's patch was introduced later.  (spwhitton)
  NOTE: 20250214: Still working on the backports.  (spwhitton)
  NOTE: 20250214: Got report that bookworm-pu FTBFS on some architectures.
  NOTE: 20250214: Will prepare second bookworm-pu to fix this.  (spwhitton)
  NOTE: 20250228: Still working on the backports.  Had to focus on Emacs this
  NOTE: 20250228: week because there was a bad arbitrary code execution CVE to
  NOTE: 20250228: fix.  (spwhitton)
--
xrdp (abhijith)
  NOTE: 20250207: Added by Front-Desk (apo)
  NOTE: 20250227: https://people.debian.org/~abhijith/upload/xrdp_patches/ (abhijith)
--
zfs-linux (dleidert)
  NOTE: 20250117: Added by Front-Desk (rouca)
  NOTE: 20250219: Follow fixes from bookworm 12.9 (CVE-2013-20001, CVE-2023-49298) (Beuc/front-desk)
--
